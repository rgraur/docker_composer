# Composer Docker image #

### Base image ###
* official [Ubuntu image](https://registry.hub.docker.com/_/ubuntu/)

### Installed packages ###
* php5
* git
* composer

### Run ###
* build image: `docker build -t composer .`
* container access: `docker run --privileged=true --volumes-from data-container --rm composer <command>`

