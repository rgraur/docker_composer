FROM ubuntu
MAINTAINER Radu Graur <radu.graur@gmail.com>

# Install Composer
RUN apt-get update && apt-get -y install curl php5 git && \
    curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    composer self-update

# Cleanup
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /opt/data
VOLUME ["/opt/data"]
WORKDIR /opt/data

ENTRYPOINT ["composer"]
CMD ["--help"]

